# Plugin Custom CSS pour Garradin

Ce plugin permet d'ajouter du code CSS personnalisé sur l'interface d'administration du logiciel de gestion d'association Garradin ( https://garradin.eu/ - https://fossil.kd2.org/garradin ).

Source : https://gitlab.com/ramoloss/garradin-plugin-custom_css

## Installation:
L'archive actuelle peut être télécharger depuis la page des [releases](https://gitlab.com/ramoloss/garradin-plugin-custom_css/-/releases).

Placer l'archive custom_css.tar.gz dans le dossier plugins de votre installation Garradin.

Puis rendez-vous dans Configuration > Extensions, sélectionnez l'extension Custom CSS et cliquez sur Installer.

Ensuite, en suivant le lien « Configurer » en face de l'extension installée, vous pouvez y entrer votre code CSS et choisir de l'activer ou non.


Seul les membres ayant les droits de configuration peuvent y avoir accès.