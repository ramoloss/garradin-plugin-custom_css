<?php

namespace Garradin\Plugin\Custom_Css;

use Garradin\UserException;
use Garradin\Utils;
use Garradin\Plugin;

class CssEdit
{
    
	private	$css_start = '/* Custom CSS plugin - start */';
	private	$css_end = '/* Custom CSS plugin - end */';

	private	$admin = '/www/admin/static/admin.css';
    private	$hand = '/www/admin/static/handheld.css';
    
    // private $plugin = new Plugin('custom_css');

	public function __construct($root = '')
	{
        // C'est crado mais jsp comment récupérer cette valeur sinon ?
        $this->admin = $root . $this->admin;
        $this->hand = $root . $this->hand;
    }
    
    public function remove($content = '')
    {
        $total = strlen($this->css_start . PHP_EOL .
			$content . PHP_EOL .
            $this->css_end);

        foreach ( [$this->admin, $this->hand] as $path)
        {
            $file = fopen($path, 'r+');
            $stat = fstat($file);
            $stat = $stat['size'];        
            
            fseek($file, $stat - 29);
            $r1 = fread($file, 29);
            fseek($file, $stat - $total);
            $r2 = fread($file, 31);
            
            if ($r1 !== $this->css_end && $r2 !== $this->css_start)
            {
                fclose($file);
                return false;
            }
            
            if (!ftruncate($file, $stat - $total))
            {
                throw new UserException('Le fichier n\'a pas pu être tronqué.');
            }
            fclose($file);
        }

        return true;
    }

    public function write($content = '')
    {
        $c = $this->css_start . PHP_EOL .
			$content . PHP_EOL .
            $this->css_end;

        foreach ( [$this->admin, $this->hand] as $path)
        {
            file_put_contents($path, $c, FILE_APPEND);
        }
        
        return true; 
    }

}