<?php

namespace Garradin;
use Garradin\Plugin\Custom_Css\CssEdit;

$session->requireAccess('config', Membres::DROIT_ADMIN);

$ok = false;

if (f('save') && $form->check('custom_css_config'))
{
	try {
		$old_content = $plugin->getConfig('css');
		$content = f('css');
		$plugin->setConfig("css", $content);
		$plugin->setConfig("enabled", f('enabled'));

		$edit = new CssEdit(ROOT);
		
		if (f('enabled'))
		{
			$edit->remove($old_content);
			$edit->write($content);
		}
		else
		{
			$edit->remove($old_content);
		}

		$ok = true;
    }
    catch (UserException $e)
    {
	    $form->addError($e->getMessage());
    }
}


$tpl->assign('ok', $ok);
$tpl->assign('enabled', $plugin->getConfig('enabled') ? 'checked':'');
$tpl->assign('css', $plugin->getConfig('css'));

$tpl->display(PLUGIN_ROOT . '/templates/config.tpl');