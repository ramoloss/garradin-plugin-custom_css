{include file="admin/_head.tpl" title="Configuration — %s"|args:$plugin.nom current="plugin_%s"|args:$plugin.id}
{* {include file="%s/templates/_menu.tpl"|args:$plugin_root current="config"} *}

{if $ok && !$form->hasErrors()}
    <p class="confirm">
        La configuration a bien été enregistrée.
    </p>
{/if}

{form_errors}

<form method="post" action="{$self_url}">
    <fieldset>
        <legend>Configuration du plugin</legend>
        <dl>
            <dt><input type="checkbox" name="enabled" id="f_enabled" {$enabled}> <label for="f_enabled">Activer/désactiver l'application du CSS</label></dt>
            <dt><label for="f_footer">Code CSS personnalisé :</label></dt>
            <dd><textarea name="css" id="f_css" cols="70" rows="5">{$css}</textarea></dd>
        </dl>
    </fieldset>

    <p class="submit">
        {csrf_field key="custom_css_config"}
        <input type="submit" name="save" value="Enregistrer &rarr;" />
    </p>
</form>

{include file="admin/_foot.tpl"}